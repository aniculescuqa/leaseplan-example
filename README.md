**Refactor:**

- removed unnecesary config files and dependencies
- added object mapper for easier parsing test data
- added negative / positive scenarios
- added scenario outline as it is easier to read and run


**Installation**
- install java and maven
- install cucumber and gherkin plugins
- clone the project
- run mvn clean install -DskipTests


**Running tests:**
- tests can be run by: 
  - running from TestRunner file
  - running the feature file
  - from terminal using mvn clean verify serenity:aggregate command to run all tests
  - mvn clean verify -Dtags="healthcheck" serenity:aggregate to run tests by tags
  - reports cand be found it target/site/serenity/index.html
  - adding new tests can be done in a new feature file by adding a feature name, a scenario name,
  and some steps by using @given/when/then/and annotations and then mapping the steps to the implementation class
  - keep in mind that any modification done in the feature/steps directories needs to be updated in the runner file,
  aswell as adding and configuring new plungins 


**CI/CD:**
  - a pipeline was created by adding the gitlab-ci.yml file in the project directory
  - the pipeline is getting the latest maven-docker image and running 4 stages: 
    - build (project copile)
    - healtcheck (to see if api is up and running before starting the tets)
    - regression (running all tests tagged with regression) 
    - report(where the artifacts from the reporting folder are moved to a public folder so they can be accessed by project members)
  - the pipeline is running per merge / every 6 hours using a scheduled cronjob

**Reports**
- to generate reports, **serenity:aggregate** needs to follow the **mvn clean verify** command
- reports can be found in the target/site/serenity/index.html file
- the file can also be found in the CI after a job has been run by 
  accessing a pipeline -> **reports** job -> **browse** button -> **public** folder -> opening **index.html** file
  https://aniculescuqa.gitlab.io/-/leaseplan-example/-/jobs/${CI_JOB_ID}/artifacts/public/index.html
