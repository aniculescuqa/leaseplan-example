package starter.data;

import lombok.Data;
import starter.response.ResponseBody;

import java.util.List;

@Data
public class TestData {
    private List<ResponseBody> responseBodyList;
}
