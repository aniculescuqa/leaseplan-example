package starter.parser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import starter.response.ResponseBody;

import java.util.List;

public class ResponseParser {

    public static List<ResponseBody> getResponseBodyList(String response) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<ResponseBody> bodyList = objectMapper.readValue(response, new TypeReference<List<ResponseBody>>() {
        });
        return bodyList;
    }
}
