package starter.response;

import lombok.Data;
import starter.constants.Constants;

@Data
public class ResponseBody {
    String provider;
    String title;
    String url;
    String brand;
    Double price;
    String unit;
    Boolean isPromo;
    String promoDetails;
    String image;

    public ResponseBody() {
        this.provider = Constants.FAKE_STRING;
        this.title = Constants.FAKE_STRING;
        this.url = Constants.FAKE_STRING;
        this.brand = Constants.FAKE_STRING;
        this.price = Constants.FAKE_DOBULE;
        this.unit = Constants.FAKE_STRING;
        this.isPromo = true;
        this.promoDetails = Constants.FAKE_STRING;
        this.image = Constants.FAKE_STRING;
    }
}
