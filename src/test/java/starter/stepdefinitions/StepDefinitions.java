package starter.stepdefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import starter.data.TestData;
import starter.parser.ResponseParser;
import starter.response.ResponseBody;

import java.io.IOException;


public class StepDefinitions {

    @Steps
    TestData testData;

    @Given("When user calls endpoint {string}")
    public void whenUserCallsEndpointUri(String string) throws JsonProcessingException {
        testData.setResponseBodyList(ResponseParser.getResponseBodyList(SerenityRest.given().get(string).thenReturn().asString()));

    }

    @When("After the endpoint is called the url param is verified to be secure")
    public void afterTheEndpointIsCalledTheEndpointTypeIsVerified() {
        testData.getResponseBodyList().forEach(r -> Assert.assertTrue(r.getUrl().contains("https")));
    }

    @Then("The test verifies the product is not free")
    public void theTestVerifiesTheUrlParamIsConsisntent() {
        testData.getResponseBodyList().forEach(r -> Assert.assertTrue(r.getPrice() > 0));
    }

    @And("The test verifies the response item {string} is as expected")
    public void theTestVerifiesTheResponseItemSizeIsAsExpected(String string) {
        Assert.assertTrue(Integer.parseInt(string) < testData.getResponseBodyList().size());
    }

    @And("The test verifies different http {string} cannot be performed on {string}")
    public void theTestVerifiesDifferentHttpMethodsCannotBePerformed(String verb, String uri) throws IOException {
        switch (verb) {
            case "patch":
                Assert.assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, SerenityRest.given().patch(uri).getStatusCode());
                break;
            case "post":
                ResponseBody r = new ResponseBody();
                int statusCode = SerenityRest.given().body(r).when().post(uri).then().extract().statusCode();
                Assert.assertTrue("the user was able to insert data", statusCode == HttpStatus.SC_METHOD_NOT_ALLOWED || statusCode == HttpStatus.SC_SERVICE_UNAVAILABLE);
                break;
            case "put":
                Assert.assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, SerenityRest.given().put(uri).getStatusCode());
                break;
        }
    }

    @Given("A healthcheck is performed for each {string} before starting other scenarios")
    public void aHealthcheckIsPerformedForEachUriBeforeStartingOtherScenarios(String string) {
        Assert.assertEquals(SerenityRest.given().get(string).thenReturn().statusCode(), HttpStatus.SC_OK);
    }
}