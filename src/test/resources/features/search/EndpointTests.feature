Feature: Endpoint tests

  @healthcheck
  Scenario Outline: Healthcheck
    Given A healthcheck is performed for each <uri> before starting other scenarios

    Examples: Endpoints
      | uri                                                              |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple" |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango" |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/water" |


  @regression
  Scenario Outline: The user verifies multiple endpoints
    Given When user calls endpoint <uri>
    When After the endpoint is called the url param is verified to be secure
    And The test verifies the response item <size> is as expected
    And The test verifies different http <methods> cannot be performed on <uri>
    Then The test verifies the product is not free

    Examples: Endpoint cases
      | uri                                                              | size  | methods |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple" | "1"  | "patch" |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango" | "2"  | "post"  |
      | "https://waarkoop-server.herokuapp.com/api/v1/search/test/water" | "3" | "put"   |

